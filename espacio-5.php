<?php
	 error_reporting(0);
	include_once("conexion.php");
	$con=new conecta;
?>
<?php include("header.php"); ?>
<body>

<!-- Pagination Buttons -->
<a href="espacio-4.php" title="Ver espacio anterior">
	<div id="page-left">
		<i class="fa fa-angle-left fa-3x"></i>
	</div>
</a>


<a href="espacio-6.php" title="Ver siguiente espacio">
	<div id="page-right">
		<i class="fa fa-angle-right fa-3x"></i>
	</div>
</a>

<!-- Main Content -->
	<!-- Menu Bar -->
	<div class="col-xs-12 static">
		<h1 class="font-sb pos-logo mt0 white"><i>UnlockSpaces <span class="woody"><h3>WoodyStyle</h3></span></i></h1>
		<div class="pos-log">
				<a href="index.php"><h5 class="intro-button inline-b font-l l-white">Home</h5></a>
				<a href="#"><h5 class="intro-button inline-b font-l l-white">Login</h5></a>
				<a href="catalogo.php" class="go-contacto"><h5 class="intro-button inline-b font-l l-white">Catálogo</h5></a>
			</div>
	</div>

	<!-- Image + Description -->
	<div class="col-xs-12 mt100 mb50">
		<div class="col-sm-6">
			<img src="imx/featured-5.jpg" class="img-responsive" alt="">
		</div>

		<div class="col-sm-6 text-center" >
			<h1 class="mb0 font-sb ">Portugal 1005, Providencia</h1>
			<p class="mt20">
				Excelente sala de reuniones ambientado en madera de roble, cubriendo además todas las necesidades requeridas para reuniones o video conferencias. Excelente sala de reuniones ambientado en madera de roble, cubriendo además todas las necesidades requeridas para reuniones o video conferencias. Excelente sala de reuniones ambientado en madera de roble, cubriendo además todas las necesidades requeridas para reuniones o video conferencias.
			</p>
			<h4 class="font-sb mt20">El lugar en palabras</h4>
			<div>
				<h4 class="inline-b"><i class="fa fa-globe white-tooltip" data-toggle="tooltip" data-placement="bottom" title="Disponible" ></i></h4>&nbsp;&nbsp;
				<h4 class="inline-b"><i class="fa fa-clock-o white-tooltip" data-toggle="tooltip" data-placement="bottom" title="De 9am a 9pm" ></i></h4>&nbsp;&nbsp;
				<h4 class="inline-b"><i class="fa fa-phone white-tooltip" data-toggle="tooltip" data-placement="bottom" title="Teléfonos" ></i></h4>&nbsp;&nbsp;
				<h4 class="inline-b"><i class="fa fa-wifi white-tooltip" data-toggle="tooltip" data-placement="bottom" title="WiFi" ></i></h4>&nbsp;&nbsp;
				<h4 class="inline-b"><i class="fa fa-wheelchair white-tooltip" data-toggle="tooltip" data-placement="bottom" title="Accesibilidad" ></i></h4>&nbsp;&nbsp;
				<h4 class="inline-b"><i class="fa fa-credit-card white-tooltip" data-toggle="tooltip" data-placement="bottom" title="Pago con tarjeta" ></i></h4>&nbsp;&nbsp;
				<h4 class="inline-b"><i class="fa fa-television white-tooltip" data-toggle="tooltip" data-placement="bottom" title="Televisores" ></i></h4>&nbsp;&nbsp;
			</div>

			<div class="text-right mt50">
				<h2 class=" mb0 font-sb ">Precio</h2>
				<h1 class="mt0 font-sb ">$6.000 / Hora</h1>
			</div>
			
		</div>
	</div>


	<!-- Map + Reservation -->
	<div class="col-xs-12">
		<div class="col-sm-4 col-sm-offset-2 mb20">
	 		<iframe id="map" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3920.552770292826!2d-71.63361284251161!3d10.69178023533602!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sve!4v1458137569007" height="450" frameborder="0" style="width:100%;"  allowfullscreen></iframe>
		</div>
		<div class="col-sm-4 text-center">
			<h2 class=" mt0 font-sb ">¡Resérvalo ya!</h2>
			<form action="espacio-1.php">
				<input type="text" class="text-style" name="rut" placeholder="RUT" required="required">
				<input type="text" class="hour-style" name="nombre" placeholder="Nombre" required="required">
				<input type="text" class="hour-style" name="apellido" placeholder="Apellido" required="required">
				<input type="email" class="text-style" name="email" placeholder="Email" required="required">
				<input type="date" class="text-style" name="date" required="required">
				<input type="text" class="hour-style" name="hora1" placeholder="Hora de inicio" required="required">
				<input type="text" class="hour-style" name="hora2" placeholder="Hora de culminación " required="required">
				<input type="text" class="text-style b-lightgray" value="San Martin 1490, Santiago Centro" name="direccion" readonly="readonly">
				<input type="text" class="hour-style b-lightgray" value="espacio-5.php" name="link" readonly="readonly">
				<input type="text" class="hour-style b-lightgray" value="$6.000" name="precio" readonly="readonly">
				<input type="submit" class="submit-style text-center " value="Reservar">
			</form>

			<!-- Catching data from form -->
			<div align="center">
	
				<?php 
					$Rut=$_GET['rut'];
					$Nombre=$_GET['nombre'];
					$Apellido=$_GET['apellido'];
					$Email=$_GET['email'];
					$Date=$_GET['date'];
					$Hora1=$_GET['hora1'];
					$Hora2=$_GET['hora2'];
					$Direccion=$_GET['direccion'];
					$Link=$_GET['link'];

					$sql="INSERT INTO `reserva`(`id_reserva`, `rut`, `nombre`, `apellido` , `email` , `fecha` , `hora1` , `hora2` , `direccion` , `link`) VALUES (NULL,'$Rut','$Nombre','$Apellido','$Email','$Date','$Hora1','$Hora2','$Direccion','$Link') ";
				
				if(mysqli_query($con->Conectarse(), $sql))
				{echo "";}
				else{echo "Los datos no pudieron ser ingresados";}




				mysqli_close($con->Conectarse());
				?>
				

			</div>
		</div>
	</div>
	


<?php include("footer.php"); ?>