<?php include("header.php"); ?>
<body style="margin:0;">

<!-- Page Intro: just visible in 1200px or higher -->
	<div class="col-xs-12 pads0 white over">
		<img src="imx/office-intro.jpg" class=" obscure alto ancho" style="position:relative;" alt="">
			<h1 class="font-sb pos-logo"><i>UnlockSpaces <span class="woody"><h3>WoodyStyle</h3></span></i></h1>
			<div class="pos-log">
				<a href="#"><h5 class="intro-button inline-b font-l l-white">Register</h5></a>
				<a href="#"><h5 class="intro-button inline-b font-l l-white">Login</h5></a>
			</div>
		<h1 class="pos-slogan text-center font-l">
			Porque nuestro compromiso es crear un excelente ambiente y hacer que su personal sienta una comodidad de hogar.
		</h1>
		<i class="fa fa-angle-down fa-5x white pos-arrow arrow"></i>
	</div>

<!-- Main Content -->

	<!-- Menu Bar -->
	<div class="col-xs-12 menu">
		<h1 class="font-sb pos-logo mt0 white"><i>UnlockSpaces <span class="woody"><h3>WoodyStyle</h3></span></i></h1>
		<div class="pos-log">
				<a href="#"><h5 class="intro-button inline-b font-l l-white">Register</h5></a>
				<a href="#"><h5 class="intro-button inline-b font-l l-white">Login</h5></a>
				<a href="catalogo.php" class="go-contacto"><h5 class="intro-button inline-b font-l l-white">Catálogo</h5></a>
			</div>
	</div>

	<!-- Featured Posts -->
	<div class="col-xs-12">
		<h1 class="text-center mt100 mb0 font-sb">¡Encuentra tu lugar ideal, resérvalo y listo!</h1>
		<h4 class="text-center font-l">Quieres algo novedoso y cómodo? Te enseñamos lo mejor de la madera.</h4>

		<h3 class="pleft40 mt50"><i class="fa fa-star brown"></i> Destacados</h3>

		<div class="col-xs-12 pads0 mt20">
			<!-- Post 1 -->
			<div class="col-md-3 mb50" align="center">
				<img src="imx/featured-1.jpg" class="img-responsive h200" alt="">
				<p class="mt20">Excelente sala de reuniones ambientado en madera de roble, cubriendo además todas las necesidades requeridas para reuniones o video conferencias.</p>
				<h5 class="font-sb">Precio: $2.000 / Hora</h5>
				<a href="espacio-1.php" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
			<!-- Post 2 -->
			<div class="col-md-3 mb50" align="center">
				<img src="imx/featured-2.jpg" class="img-responsive h200" alt="">
				<p class="mt20">Cómoda sala de espera, indicada para aquellos que necesiten un lugar cómodo mientras esperan para ser atendidos, cuenta con diferentes ambientes.</p>
				<h5 class="font-sb">Precio: $3.000 / Hora</h5>
				<a href="espacio-2.php" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
			<!-- Post 3 -->
			<div class="col-md-3 mb50" align="center">
				<img src="imx/featured-3.jpg" class="img-responsive h200" alt="">
				<p class="mt20">Perfectas estaciones de trabajo para aquellos que trabajan en equipo y necesiten de un lugar cómodo en donde necesiten que sus ideas fluyan, ambientizado con madera de pino.</p>
				<h5 class="font-sb">Precio: $4.000 / Hora</h5>
				<a href="espacio-3.php" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
			<!-- Post 4 -->
			<div class="col-md-3 mb50" align="center">
				<img src="imx/featured-4.jpg" class="img-responsive h200" alt="">
				<p class="mt20">Especial para trabajar y descansar, esta estación de trabajo / sala de relajación combina a la perfección lo que nosotros llamamos "un trabajo relajado".</p>
				<h5 class="font-sb">Precio: $5.000 / Hora</h5>
				<a href="espacio-4.php" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
		</div>
	</div>

	<!-- How it works -->
	<div class="col-xs-12 b-brown">
		<h1 class="white text-center mt50 mb0 font-sb">¿No sabes cómo reservar?</h1>
		<h4 class="white font-l text-center mb50">¡Déjanos ayudarte! En solo 4 sencillos pasos</h4>

		<div class="col-xs-12 white">
			<div class="col-md-3 mb50">
				<img src="imx/5.png" class="img-responsive mb20" alt="">
				<h1 class="inline-b">1°</h1><h3 class="inline-b">Busca tu espacio ideal</h3>
				<p>Antes que todo revisa nuestro catálogo y busca el espacio que mas se adecúe a tus necesidades y a la de tus empleados o invitados.</p>
			</div>
			<div class="col-md-3 mb50">
				<img src="imx/1.png" class="img-responsive mb20" alt="">
				<h1 class="inline-b">2°</h1><h3 class="inline-b">Resérvalo</h3>
				<p>Cuando encuentres tu espacio ideal, indica la fecha y la hora en que la sala estará en uso y ¡listo!.</p>
			</div>
			<div class="col-md-3 mb50">
				<img src="imx/2.png" class="img-responsive mb20" alt="">
				<h1 class="inline-b">3°</h1><h3 class="inline-b">Confirmación</h3>
				<p>Cuando reserves, contactaremos al dueño de la sala para notificarle el dia, hora y método de pago a realizar.</p>
			</div>
			<div class="col-md-3 mb50">
				<img src="imx/3.png" class="img-responsive mb20" alt="">
				<h1 class="inline-b">4°</h1><h3 class="inline-b">¡Disfrútalo!</h3>
				<p>Ya está todo listo para que puedas realizar tus reuniones, organizar tu equipo de trabajo y ¡mucho más!.</p>
			</div>
		</div>
	</div>

	<!-- Book your ideal space -->
	<div class="col-xs-12" align="center">
		<h1 class=" mt50 mb0 font-sb">¿Buscas un espacio en particular?</h1>
		<h4 class="font-l  mb50">ve a nuestro catálogo y encuentra exactamente lo que buscas</h4>
		<a href="catalogo.php" class="submit-style" style="width:10%; margin-left:2.5%; padding:10px;">Ir a Catálogo</a>	
	</div>

	

<?php include("footer.php"); ?>