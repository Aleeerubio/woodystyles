<?php include("header.php"); ?>
<body>

<!-- Main Content -->
	<!-- Menu Bar -->
	<div class="col-xs-12 static">
		<h1 class="font-sb pos-logo mt0 white"><i>UnlockSpaces <span class="woody"><h3>WoodyStyle</h3></span></i></h1>
		<div class="pos-log">
				<a href="#"><h5 class="intro-button inline-b font-l l-white">Register</h5></a>
				<a href="#"><h5 class="intro-button inline-b font-l l-white">Login</h5></a>
				<a href="index.php" class="go-contacto"><h5 class="intro-button inline-b font-l l-white">Home</h5></a>
			</div>
	</div>

	
	<div class="col-xs-12">
		<h1 class="text-center mt100 mb0 font-sb">¡Busca lo que mejor se acomoda a ti!</h1>
		<h4 class="text-center font-l">¡Busca el precio que más se ajuste a tu bolsillo!</h4>

		<!-- Featured Posts-->
		<h3 class="pleft40 mt50"><i class="fa fa-star brown"></i> Destacados</h3>
		<div class="col-xs-12 pads0 mt20">
			<div class="col-md-6 mb50" align="center">
				<img src="imx/featured-1.jpg" class="img-responsive h350" alt="">
				<p class="mt20">Excelente sala de reuniones ambientado en madera de roble, cubriendo además todas las necesidades requeridas para reuniones o video conferencias.</p>
				<h5 class="font-sb">Precio: $2.000 / Hora</h5>
				<a href="espacio-1.php" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
			<div class="col-md-6 mb50" align="center">
				<img src="imx/featured-2.jpg" class="img-responsive h350" alt="">
				<p class="mt20">Cómoda sala de espera, indicada para aquellos que necesiten un lugar cómodo mientras esperan para ser atendidos, cuenta con diferentes ambientes.</p>
				<h5 class="font-sb">Precio: $3.000 / Hora</h5>
				<a href="espacio-2.php" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
			<div class="col-md-6 mb50" align="center">
				<img src="imx/featured-3.jpg" class="img-responsive h350" alt="">
				<p class="mt20">Perfectas estaciones de trabajo para aquellos que trabajan en equipo y necesiten de un lugar cómodo en donde necesiten que sus ideas fluyan, ambientizado con madera de pino.</p>
				<h5 class="font-sb">Precio: $4.000 / Hora</h5>
				<a href="espacio-3.php" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
			<div class="col-md-6 mb50" align="center">
				<img src="imx/featured-4.jpg" class="img-responsive h350" alt="">
				<p class="mt20">Especial para trabajar y descansar, esta estación de trabajo / sala de relajación combina a la perfección lo que nosotros llamamos "un trabajo relajado".</p>
				<h5 class="font-sb">Precio: $5.000 / Hora</h5>
				<a href="espacio-4.php" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
		</div>
	</div>
	
	<!-- Search Field -->
	<div class="col-xs-12 mt20" align="center">
		<h1 class="text-center mt0 mb0 font-sb">Encuentra tu espacio ideal</h1>
		<form action="busqueda.php">
			<h4 class="font-l inline-b">Consuta por rango de precio:</h4><br>
			<h5 class="font-l inline-b">Desde:</h5>
			<select name="rango1" class="mt20"> 
				<option value="2000">$2.000</option>
				<option value="3000">$3.000</option>
				<option value="4000">$4.000</option>
				<option value="5000">$5.000</option>
				<option value="6000">$6.000</option>
				<option value="7000">$7.000</option>
				<option value="8000">$8.000</option>
				<option value="9000">$9.000</option>
				<option value="10000">$10.000</option>
			</select>
			<h5 class="font-l inline-b">Hasta:</h5>
			<select name="rango2" class="mt20"> 
				<option value="2000">$2.000</option>
				<option value="3000">$3.000</option>
				<option value="4000">$4.000</option>
				<option value="5000">$5.000</option>
				<option value="6000">$6.000</option>
				<option value="7000">$7.000</option>
				<option value="8000">$8.000</option>
				<option value="9000">$9.000</option>
				<option value="10000">$10.000</option>
			</select><br>
			<input type="submit" class="submit-style text-center mt20" style="width:15%;" value="Buscar">
		</form>
	</div>
<?php include("footer.php"); ?>