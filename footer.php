<!-- Reservations -->
	<div class="col-xs-10 col-xs-offset-1 mt50 separator"></div>

	<div class="col-xs-12 mt50" id="BuscaReserva">
		<h1 class=" text-center mt50 mb0 font-sb">¿Ya reservaste?</h1>
		<h4 class=" font-l text-center mb20">Utliza nuestro consultor y revisa tu reserva</h4>

		<form action="reservas.php" name="datos" method="get" class="text-center" id="reservas">
			<h4 class="inline-b">Consulta tu reserva:</h4>
			<input type="text" placeholder="Ingrese su RUT" name="consulta" class="text-style" style="width:20%;" required>
			<input type="submit" value="Consultar" class="submit-style" style="width:10%; padding-left:0; margin-left:2.5%;">

			<br><br><br><br>
		</form>
	</div>

<!-- Footer -->
	<footer>
		<div class="col-xs-12 b-woody">
			<div class="col-sm-3 col-xs-6 white mt50 mb50">
				<h2 class="font-sb mt0"><i>UnlockSpaces <span class="woody"><h4>WoodyStyle</h4></span></i></h2>
				<h5 class="text-right font-l mb0">En afiliación con <a href="#" class="l-white font-sb">UnlockSpaces</a></h5>
				<h5 class="text-right font-l mt0">Una creacion de <i class="font-n">Shareable Innovation Spa</i></h5>
				<h5 class="text-right font-l">
					<i>"Porque nuestro compromiso es crear un excelente ambiente y hacer que su personal sienta una comodidad de hogar."</i>
				</h5>
			</div>
			<div class="col-sm-6 hidden-xs mt50">
				<h5 class="text-center font-l white pos-rights mt100"> ©<i>UnlockSpaces WoodyStyle 2016. Todos los derechos reservados.</i></h5>
			</div>
			<div class="col-sm-3 col-xs-6 mt50 mb50 white" align="center">
				<h4 class="font-l bord-bot mt0">Mantengámonos en contacto!</h4>
				<h4 id="social">
					<span  data-toggle="tooltip" data-placement="bottom" title="@UnlockSpaces">
						<a href="#" class="inline-b"><i class="fa fa-instagram"></i></a>&nbsp;
					</span>

					<span  data-toggle="tooltip" data-placement="bottom" title="ShareableInnovations">
						<a href="#" class="inline-b"><i class="fa fa-youtube-play"></i></a>&nbsp;
					</span>

					<span  data-toggle="tooltip" data-placement="bottom" title="Shareable Innovations: UnlockSpaces">
						<a href="#" class="inline-b"><i class="fa fa-facebook-square"></i></a>&nbsp;
					</span>

					<span  data-toggle="tooltip" data-placement="bottom" title="@UnlockSpacesWoody">
						<a href="#" class="inline-b"><i class="fa fa-twitter"></i></a>&nbsp;
					</span>
	 			</h4>
	 			<h6 class="font-l mt20">
	 				Diseñado, maquetado y programado por: <br> <span class="font-n">Alejandro Rubio | Pasaporte: 090923106</span>
	 				<br> <i>alejandro.david.rubio@gmail.com</i>
	 				<br> +56 9 7880 1597
	 			 </h6>
	 			 <h6 class="font-l">Para <span class="font-n">Shareable Innovations</span> con cariño &nbsp;<i class="fa fa-heart"></i></h6>
			</div>
		</div>
	</footer>
	
	<!-- Boton de ir hacia arriba -->
	<a href="#" class="go-top"><i class="fa fa-home fa-2x"></i></a>
	
</body>

<!------------------ Scripts ---------------------->

<script src="js/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script>

// Intro Size Script
function sizesnow(){ // CALCULAMOS EN UNA FUNCION LA ALTURA Y EL LIMITE DE ANCHO
  alto = $( window ).height();
  ancho = $( window ).width();
 
    $(".altox").height(alto); 
    $(".alto").height(alto);
    if(ancho>998){
    $(".ancho").width(ancho); 
	}    
}
sizesnow(); //Execute

// Arrow animation
$('.arrow').click(function(event) {
        event.preventDefault();
        
        $('html, body').animate({scrollTop: 668}, 900);
      })


// NavBar animation
var num = 667; //number of pixels before modifying styles
  $(window).bind('scroll', function () {
      if ($(window).scrollTop() > num) {
          $('.menu').addClass('fixed');
      } else {
          $('.menu').removeClass('fixed');
      }
  });

// bootstrap tooltip
 $(function () {
   $('[data-toggle="tooltip"]').tooltip()
 })

// go to start animation
$(document).ready(function() {
	// Show or hide the sticky footer button
	$(window).scroll(function() {
		if ($(this).scrollTop() > 660) {
			$('.go-top').fadeIn(400);
		} else {
			$('.go-top').fadeOut(200);
		}
	});
			
	// Animate the scroll to top
	$('.go-top').click(function(event) {
		event.preventDefault();
		$('html, body').animate({scrollTop: 0}, 1000);
	})
});


</script>
</html>