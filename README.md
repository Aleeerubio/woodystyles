Hello! This is my progress in the test...

First of all I made a huge first commit because I forgot to commit the other processes while I was coding, sorry for that, now I'm focused about this too and I will commit all the changes so you can see every change that I did.

As you can see, I decided to make a "woody" version of your website UnlockSpaces, the idea of this test for me is not only to show you all my skills with a language, is also to show you how many languages and frameworks I can handle, although I'm not using Angular.

I´m using for this website: 
-HTML5 (Body) 
-CSS3 (Style) 
-JavaScript (Some calculations and animations) 
-PHP (Data management) 
-MySql (Database) 
-PhpMyAdmin(DB manager) 
-Jquery (Animations) 
-Bootstrap (Responsive Design for every type of screen) 
-Knowledge about responsive design and UI.

So what I want in this test is to show you all the things that I know, I can go deeper with some languages, but I prefer to show you a little bit of all, so you can see that I fulfill all the requirementes that you need, also I want to learn more about these lenguages to go as deeper as possible.

And yes, I really really want to learn Angular too, and I will.

So I hope that you are enjoying the website, I'm proud of it!

Thanks, Alejandro Rubio