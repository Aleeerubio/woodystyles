-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-07-2016 a las 21:49:09
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `woody`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `espacio`
--

CREATE TABLE IF NOT EXISTS `espacio` (
`id_espacio` int(11) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `precio` varchar(20) NOT NULL,
  `link` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `espacio`
--

INSERT INTO `espacio` (`id_espacio`, `direccion`, `descripcion`, `precio`, `link`) VALUES
(1, 'San Martin 1490, Santiago Centro', 'Excelente sala de reuniones ambientado en madera de roble, cubriendo ademas todas las necesidades requeridas para reuniones o video conferencias.', '2000', 'espacio-1.php'),
(2, 'Francisco Bilbao 1427, Las Condes', 'Comoda sala de espera, indicada para aquellos que necesiten un lugar comodo mientras esperan para ser atendidos, cuenta con diferentes ambientes.', '3000', 'espacio-2.php'),
(3, 'Alameda 453, Santiago Centro', 'Perfectas estaciones de trabajo para aquellos que trabajan en equipo y necesiten de un lugar comodo en donde necesiten que sus ideas fluyan, ambientizado con madera de pino', '4000', 'espacio-3.php'),
(4, 'Portugal 1005, Providencia', 'Especial para trabajar y descansar, esta estacion de trabajo / sala de relajacion combina a la perfeccion lo que nosotros llamamos "un trabajo relajado"', '5000', 'espacio-4.php'),
(5, 'Providencia 105, Providencia', 'Excelente sala de reuniones ambientado en madera de roble, cubriendo ademas todas las necesidades requeridas para reuniones o video conferencias', '6000', 'espacio-5.php'),
(6, 'Independecia 605, Independencia', 'Comoda sala de espera, indicada para aquellos que necesiten un lugar comodo mientras esperan para ser atendidos, cuenta con diferentes ambientes.', '7000', 'espacio-6.php'),
(7, 'Apoquindo 758, Las Condes', 'Perfectas estaciones de trabajo para aquellos que trabajan en equipo y necesiten de un lugar comodo en donde necesiten que sus ideas fluyan, ambientizado con madera de pino', '8000', 'espacio-7.php'),
(8, 'Diagonal Paraguay 24, Santiago Centro', 'Especial para trabajar y descansar, esta estacion de trabajo / sala de relajacion combina a la perfeccion lo que nosotros llamamos "un trabajo relajado"', '9000', 'espacio-8.php');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE IF NOT EXISTS `reserva` (
`id_reserva` int(11) NOT NULL,
  `rut` varchar(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora1` varchar(30) NOT NULL,
  `hora2` varchar(30) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `precio` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `espacio`
--
ALTER TABLE `espacio`
 ADD PRIMARY KEY (`id_espacio`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
 ADD PRIMARY KEY (`id_reserva`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `espacio`
--
ALTER TABLE `espacio`
MODIFY `id_espacio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `reserva`
--
ALTER TABLE `reserva`
MODIFY `id_reserva` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=124;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
